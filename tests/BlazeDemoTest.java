import static org.junit.Assert.*;

import java.util.List;

import javax.swing.text.html.Option;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class BlazeDemoTest {
	final String CHROMEDRIVER_LOCATION = "/Users/macstudent/Desktop/chromedriver";
	// Website we want to test
	final String URL_TO_TEST = "http://blazedemo.com/";
	
	// -----------------------------------
	// Global driver variables
	// -----------------------------------
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", CHROMEDRIVER_LOCATION);
		driver = new ChromeDriver();
		
		//  go to website
		driver.get(URL_TO_TEST);
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(5000);
		driver.close();
	}

	@Test
	public void testcase1() {
		
		List<WebElement> cities = driver.findElements(
				By.cssSelector(".form-inline"));
		
		System.out.println("Number of cities on page: " + cities.size());
		
		
		
		
		assertEquals(7, cities.size());
		
	}
	
	@Test 
	public void testcase2(){
		
		Select drpCountry1 = new Select(driver.findElement(By.name("fromPort")));
		drpCountry1.selectByVisibleText("Paris");
		
		Select drpCountry2 = new Select(driver.findElement(By.name("toPort")));
		drpCountry2.selectByVisibleText("Rome");
		
		WebElement Btn = driver.findElement(
				By.className("btn btn-primary"));
		// - PUSH the button
		Btn.click();
		
	}

}
