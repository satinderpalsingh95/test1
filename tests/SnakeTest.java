import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTest {
	Snake pSnake;
	Snake tSnake;

	@Before
	public void setUp() throws Exception {
		 pSnake = new Snake("Peter", 10, "coffee");
		 tSnake = new Snake("Takis",80,"vegetables");
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testcase1() {
		//Snake pSnake = new Snake("Peter", 10, "coffee");
		//Snake tSnake = new Snake("Takis",80,"vegetable");
		pSnake.isHealthy();
		tSnake.isHealthy();	
		//assertEquals(0, pSnake.isHealthy());
		assertTrue(tSnake.isHealthy());
		assertFalse(pSnake.isHealthy());
		
		
	}
	
	@Test
	public void testcase2() {
		//Snake pSnake = new Snake("Peter", 10, "coffee");
		//Snake tSnake = new Snake("Takis",80,"vegetable");
		
		assertTrue(pSnake.fitsInCage(10));
		assertTrue(pSnake.fitsInCage(11));
		assertFalse(pSnake.fitsInCage(9));
		
		
	}

}
